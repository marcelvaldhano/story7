from django.test import TestCase,Client
from django.urls import resolve
from .views import *

# Create your tests here.
class TDD(TestCase):
    def test_story7_is_exist(self):
        response=Client().get('')
        self.assertEqual(response.status_code,200)
        
    def test_story7_using_story7_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'story7.html')
    
    def test_story7_using_story7_func(self):
        found = resolve('/')
        self.assertEqual(found.func, story7)
    
    def test_ada_kata(self):
        response=Client().get('')
        content=response.content.decode("utf8")
        self.assertIn("Hi! My name is Marcel Valdhano.",content)
